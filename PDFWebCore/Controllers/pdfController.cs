﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PDFWebCore.helper;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFWebCore.Controllers
{
    public class pdfController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public pdfController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        [HttpGet("api/pdf")]
        public IActionResult Index()
        {
            string v = this.RenderViewAsync<string>("tickets", "").Result;
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait; 

            // create a new pdf document converting an html string
            PdfDocument doc = converter.ConvertHtmlString(v, "");

            // save pdf document
            byte[] rep = doc.Save();
            return File(rep, "application/pdf");
        }

        [HttpGet("api/image/{nameWithExtension}")]
        public IActionResult GetImage(string nameWithExtension)
        {
            string ext = nameWithExtension.Split(".")[1];
            string webRootPath = _hostingEnvironment.WebRootPath;
            Byte[] b = System.IO.File.ReadAllBytes(@$"{webRootPath}\assets\{nameWithExtension}");
            return File(b, $"image/{ext}");
        }
    }
}
